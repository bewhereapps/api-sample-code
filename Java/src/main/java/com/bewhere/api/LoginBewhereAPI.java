package com.bewhere.api;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.binary.Hex;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

class Credentials {
	public String username;
	public String authphrase;
	public int tidHashAlgorithm;
	public int tidSession;
}

class SnapshotUpdateOdometer {
	public int distance;
	public long timestamp;
}

class SnapshotUpdateTotalEngineHrs {
	public int total_engine_hrs;
	public long timestamp;
}

public class LoginBewhereAPI {

	static String API_URL = "https://api.bewhere.com";
	static JsonObject token;
	static JsonObject ftoken;
	static JsonParser jsonParser = new JsonParser();
	static Gson gson = new Gson();

	static String username = "your username";
	static String password = "your password";
	static String accountId = "your account id";
	
	static Client client = Client.create();
    static String myToken = "";
	public static void main(String[] args) {
		
        /**** If you don't have API key, use login method to get temporary token, the token will expire if the token has not been used in 20 minutes ****/
		login();
        //Get a list of all the devices
		//Please refer to: https://app.swaggerhub.com/apis/BEWHEREINC_1/BeWhere/4.0.0#/Beacons/get_accounts__accountKey__beacons
        getBeacons(accountId);
        //Get all devices snapshot
        //please refer to: https://app.swaggerhub.com/apis/BEWHEREINC_1/BeWhere/4.0.0#/Snapshots/get_accounts__accountKey__snapshots
        getSnapshot(accountId);
        //Get device 359215101450097 historical records based on device time stamp. start and end are Unix epoch time in milliseconds.
        //Please refer to:  https://app.swaggerhub.com/apis/BEWHEREINC_1/BeWhere/4.0.0#/Streams/get_accounts__accountKey__streams__id_
        String deviceId = "359215101450097";
        long start = System.currentTimeMillis() - 24 * 3600 * 1000;
        long end = System.currentTimeMillis() ;
        getDeviceHistory(accountId, deviceId, start, end);
        //Get all devices historical records  based on server receive time. start and end are Unix epoch time in milliseconds.
        //Please refer to: https://app.swaggerhub.com/apis/BEWHEREINC_1/BeWhere/4.0.0#/Streams/get_accounts__accountKey__streams_history
        getAllDevicesHistory(accountId, start, end);
         //Move device 100000000101 to new account 
        String currentAccountId = "Current AccountId";
        String newAccountId = "New AccountId";
        moveListofBeacons(currentAccountId, newAccountId, new String[] {"100000000101"});
        //Get Device snapshot by device name.
       //Please refer to: https://app.swaggerhub.com/apis/BEWHEREINC_1/BeWhere/4.0.0#/Snapshots/get_accounts__accountKey__snapshots_name__name_       
        getSnapshotByName(accountId, "device name");
        //Update device distance
        //Please refer to: https://app.swaggerhub.com/apis/BEWHEREINC_1/BeWhere/4.0.0#/Snapshots/put_accounts__accountKey__snapshots__id_       
        SnapshotUpdateOdometer odometer = new SnapshotUpdateOdometer();
        odometer.distance = 1000; //1000 meters
        odometer.timestamp = 1651853524000l; //Epoch time in milliseconds
        updateOdometer(accountId, "357591080080689", odometer);
        //Update device total engine hours
        //Please refer to: https://app.swaggerhub.com/apis/BEWHEREINC_1/BeWhere/4.0.0#/Snapshots/put_accounts__accountKey__snapshots__id_       
        SnapshotUpdateTotalEngineHrs totalEngineHrs = new SnapshotUpdateTotalEngineHrs();
        totalEngineHrs.total_engine_hrs = 3000; //3000 seconds
        totalEngineHrs.timestamp = 1651853524000l; //Epoch time in milliseconds
        updateTotalEngineHrs(accountId, "357591080080689", totalEngineHrs);
        
        /****end ****/
        
        
        /**** If you have API key, then you can use accountid and api key to send request without calling login method ****/
        String apiKey = "your api key";
        myToken = apiKey;
        //Get a list of all the devices
		//Please refer to: https://app.swaggerhub.com/apis/BEWHEREINC_1/BeWhere/4.0.0#/Beacons/get_accounts__accountKey__beacons
        getBeacons(accountId);
        //Get all devices snapshot
        //please refer to: https://app.swaggerhub.com/apis/BEWHEREINC_1/BeWhere/4.0.0#/Snapshots/get_accounts__accountKey__snapshots
        getSnapshot(accountId);
        //Get device 359215101450097 historical records based on device time stamp. start and end are Unix epoch time in milliseconds.
        //Please refer to:  https://app.swaggerhub.com/apis/BEWHEREINC_1/BeWhere/4.0.0#/Streams/get_accounts__accountKey__streams__id_
        deviceId = "359215101450097";
        start = System.currentTimeMillis() - 24 * 3600 * 1000;
        end = System.currentTimeMillis() ;
        getDeviceHistory(accountId, deviceId, start, end);
        //Get all devices historical records  based on server receive time. start and end are Unix epoch time in milliseconds.
        //Please refer to: https://app.swaggerhub.com/apis/BEWHEREINC_1/BeWhere/4.0.0#/Streams/get_accounts__accountKey__streams_history
        getAllDevicesHistory(accountId, start, end);        
        //Move device 100000000101 to new account 
        //Please refer to: https://app.swaggerhub.com/apis/BEWHEREINC_1/BeWhere/4.0.0#/Beacons/put_accounts__accountKey__beacons_multi_move__newaccountKey_
       currentAccountId = "Current AccountId";
       newAccountId = "New AccountId";
       moveListofBeacons(currentAccountId, newAccountId, new String[] {"100000000101"});  
       //Get Device snapshot by device name.
       //Please refer to: https://app.swaggerhub.com/apis/BEWHEREINC_1/BeWhere/4.0.0#/Snapshots/get_accounts__accountKey__snapshots_name__name_       
       getSnapshotByName(accountId, "device name");
       //Update device distance
       //Please refer to: https://app.swaggerhub.com/apis/BEWHEREINC_1/BeWhere/4.0.0#/Snapshots/put_accounts__accountKey__snapshots__id_       
       odometer = new SnapshotUpdateOdometer();
       odometer.distance = 1000; //1000 meters
       odometer.timestamp = 1651853524000l; //Epoch time in milliseconds
       updateOdometer(accountId, "357591080080689", odometer);
       //Update device total engine hours
       //Please refer to: https://app.swaggerhub.com/apis/BEWHEREINC_1/BeWhere/4.0.0#/Snapshots/put_accounts__accountKey__snapshots__id_       
       totalEngineHrs = new SnapshotUpdateTotalEngineHrs();
       totalEngineHrs.total_engine_hrs = 3000; //3000 seconds
       totalEngineHrs.timestamp = 1651853524000l; //Epoch time in milliseconds
       updateTotalEngineHrs(accountId, "357591080080689", totalEngineHrs);
 
	}

	static private void login() {
		String token_url = API_URL + "/authentication/" + username + "?type=M2M";
		WebResource webResource = client.resource(token_url);

		ClientResponse response = webResource.accept("application/json").get(ClientResponse.class);
		token = jsonParser.parse(response.getEntity(String.class)).getAsJsonObject();
		//the token is temporary token will expire if it has not been used in 20 minutes
		getToken();
		
	}

	static private String getHashString(String str) {
		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("SHA-256");
			byte[] hash = digest.digest(str.getBytes(StandardCharsets.UTF_8));
			return Hex.encodeHexString(hash);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}

	static void getToken() {
		String token_url = API_URL + "/authentication";
		WebResource webResource = client.resource(token_url);

		String hashedHex = getHashString(password);
		String finalHex = getHashString(token.get("salt").getAsString() + hashedHex);

		Credentials toSend = new Credentials();
		toSend.username = username;
		toSend.authphrase = finalHex;
		toSend.tidHashAlgorithm = 2; // SHE_256
		toSend.tidSession = 3;

		ClientResponse response = webResource.accept("application/json")
				.header("Token", token.get("token").getAsString()).header("Content-Type", "application/json")
				.post(ClientResponse.class, gson.toJson(toSend));
		ftoken = jsonParser.parse(response.getEntity(String.class)).getAsJsonObject();
		System.out.println(ftoken);
		myToken = token.get("token").getAsString();

	}
	
	static void getBeacons(String accountId) {
		String token_url = API_URL + "/accounts/" + accountId + "/beacons";
		WebResource webResource = client.resource(token_url);

		ClientResponse response = webResource.accept("application/json")
				.header("Token", myToken).header("Content-Type", "application/json")
				.get(ClientResponse.class);
		System.out.println(response.getEntity(String.class));
	}
	
	static void getSnapshot(String accountId) {
		String token_url = API_URL + "/accounts/" + accountId + "/snapshots";
		WebResource webResource = client.resource(token_url);

		ClientResponse response = webResource.accept("application/json")
				.header("Token", myToken).header("Content-Type", "application/json")
				.get(ClientResponse.class);
		System.out.println(response.getEntity(String.class));
	}
	
	static void getDeviceHistory(String accountId, String deviceId, long start, long end) {
		String token_url = API_URL + "/accounts/" + accountId + "/streams/" + deviceId + "?start=" + start +  "&end="+ end;
		WebResource webResource = client.resource(token_url);

		ClientResponse response = webResource.accept("application/json")
				.header("Token", myToken).header("Content-Type", "application/json")
				.get(ClientResponse.class);
		System.out.println(response.getEntity(String.class));
	}
	
	static void getAllDevicesHistory(String accountId, long start, long end) {
		String token_url = API_URL + "/accounts/" + accountId + "/streams/history?limit=1000&start=" + start + "&end=" + end;
		WebResource webResource = client.resource(token_url);

		ClientResponse response = webResource.accept("application/json")
				.header("Token", myToken).header("Content-Type", "application/json")
				.get(ClientResponse.class);
		System.out.println(response.getEntity(String.class));
	}
	
	static void moveListofBeacons(String accountid, String newAccountid, String[] beacons) {
		 String token_url = API_URL + "/accounts/" + accountid + "/beacons/multi/move/" + newAccountid;
		 WebResource webResource = client.resource(token_url);
		 ClientResponse response = webResource.accept("application/json")
					.header("Token", myToken).header("Content-Type", "application/json")
					.put(ClientResponse.class, gson.toJson(beacons));
		 System.out.println(response.getStatus());
	}
	
	static void getSnapshotByName(String accountId, String name) {
		String token_url = API_URL + "/accounts/" + accountId + "/snapshots/name/" + name;
		WebResource webResource = client.resource(token_url);

		ClientResponse response = webResource.accept("application/json")
				.header("Token", myToken).header("Content-Type", "application/json")
				.get(ClientResponse.class);
		System.out.println(response.getEntity(String.class));
	}
	
	static void updateOdometer(String accountId, String id, SnapshotUpdateOdometer odometer) {
		String token_url = API_URL + "/accounts/" + accountId + "/snapshots/" + id;
		WebResource webResource = client.resource(token_url);

		ClientResponse response = webResource.accept("application/json")
				.header("Token", myToken).header("Content-Type", "application/json")
				.put(ClientResponse.class, gson.toJson(odometer));
		
		System.out.println(response.getStatus());
	}
	
	static void updateTotalEngineHrs(String accountId, String id, SnapshotUpdateTotalEngineHrs totalEngineHrs) {
		String token_url = API_URL + "/accounts/" + accountId + "/snapshots/" + id;
		WebResource webResource = client.resource(token_url);

		ClientResponse response = webResource.accept("application/json")
				.header("Token", myToken).header("Content-Type", "application/json")
				.put(ClientResponse.class, gson.toJson(totalEngineHrs));
		
		System.out.println(response.getStatus());
	}
}
