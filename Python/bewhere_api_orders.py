import hashlib
import requests
import time
import json

class Bewhere:
  def __init__(self, token, accountKey):
    self.accountKey = accountKey
    self.baseURL = "https://api.bewhere.com"
    self.headers = {'Token': token,
      'Accept': 'application/json',
      'Content-Type' : 'application/json'}

  #create new order
  def newOrder(self):
    postUrl = self.baseURL + "/accounts/" + self.accountKey + "/orders" 
    print postUrl

    data = {"deviceType":"BeTenPlus",
        "quantity":15,
        "requestDate":"2024-08-27 13:10:20", #format:yyyy-MM-dd HH:mm:ss
        "traceId":"trace-1234561",
		"shippingDetails":{
		  "contactPhone":"4161",
		  "contactName":"John",
		  "contactEmail":"contactEmail12@bewhere.com",
		  "addressLineOne":"finch avenue",
		  "addressLineTwo":"suit 300",
		  "city":"Toronto",
		  "state":"ON",
		  "zip":"94301"
		}
	}
 
    response = requests.post(postUrl, json=data, headers=self.headers)  
    print response.status_code	
    print response.json()
	
  #get order by id 
  def getOrder(self, orderId):
    postUrl = self.baseURL + "/accounts/" + self.accountKey + "/orders/" + orderId 
    print postUrl

    response = requests.get(postUrl, headers=self.headers)    
    print response.status_code
    print response.json()
	
  #cancel order
  def cancelOrder(self, orderId):
    postUrl = self.baseURL + "/accounts/" + self.accountKey + "/orders/" + orderId 
    print postUrl

    response = requests.delete(postUrl, headers=self.headers)    
    print response.status_code
	
	
api = Bewhere(your token, your accountid)
#api.newOrder();
#api.getOrder("H2VW8TWaEh1729798437592");
#api.cancelOrder("H2VW8TWaEh1729798437592");
