import hashlib
import requests
import time
import json

class Bewhere:
  ## Sets up the local variables for the username/password
  def __init__(self, username, password, key):
    self.username=username
    self.password=password
    self.accountKey = key

    self.baseURL = "https://api.bewhere.com"
    self.token = ""
 
  def authentication(self):
       
    url = self.baseURL + "/authentication/" + self.username + "?type=m2m"
    response = requests.get(url)
    response_data = response.json()

    hashGen = hashlib.sha256()

    # Hash our password
    a = bytearray()
    a.extend(self.password.encode())	####encode the password hashgen request for encoded data
    hashGen.update(a)
    first_hash = hashGen.hexdigest()

    # Add it to the salt
    b = bytearray()
    concat = response_data["salt"]+first_hash
    b.extend(concat.encode())
    hashGen_final = hashlib.sha256()
    hashGen_final.update(b)
    final_hash = hashGen_final.hexdigest()

    self.token = response_data["token"]

    # Post data to send back
    data = {'authphrase' : final_hash,
        'username' : self.username,
        'tidHashAlgorithm' : '2',
        'tidSession' : '3'}
       
    # Set the standard headers to include the token
    self.headers = {'Token': self.token,
    'Accept': 'application/json',
    'Content-Type' : 'application/json'}

    post_url = self.baseURL + "/authentication"
    response = requests.post(post_url, json=data, headers=self.headers)
    
    print (response.text)
    #print response.json()

  def setTimerConfig(self, deviceId):
    postUrl = self.baseURL + "/accounts/" + self.accountKey + "/beacons/configuration/" + deviceId
    #print postUrl

    #timer config
    data = {"deviceMode":1,
        "workingMode":0,
        "sensor":0,
        "interval":900, #15 minutes in seconds
		"gpsSkip":False, #only available for firmware version 1.6.0 and above
        "interruptConfig":{"inner":0,"outter":0,"actThresh":0,"actTime":0,"inactThresh":0,"inactTime":0}}
	
    response = requests.post(postUrl, json=data, headers=self.headers)    
    print (response.text)

  def setMotionMovement(self, deviceId):
    postUrl = self.baseURL + "/accounts/" + self.accountKey + "/beacons/configuration/" + deviceId
    #print postUrl

    #Device does not send Motion Start/stop events
    data = {"deviceMode":1,
        "workingMode":1,
        "sensor":1,
        "interval":900, #15 minutes in seconds
        "interruptConfig":{"inner":5,"outter":12,"actThresh":300,"actTime":32,"inactThresh":256,"inactTime":64}} 
        #outter = (Stationary inerterval)/interval
 
    response = requests.post(postUrl, json=data, headers=self.headers)    
    print (response.text)

  def setMotionTrip(self, deviceId):
    postUrl = self.baseURL + "/accounts/" + self.accountKey + "/beacons/configuration/" + deviceId
    #print postUrl

    #Device sends Motion Start/stop + Motion Continuous events
    data = {"deviceMode":1,
        "workingMode":3,
        "sensor":1,
        "interval":900, #15 minutes in seconds,
		"continousMotion": True, #Enable continuous motion for motion start/stop configuration, only available for firmware version 1.6.0 and above 
		"interruptConfig":{"inner":5,"outter":12,"actThresh":300,"actTime":32,"inactThresh":256,"inactTime":64}}
        #outter = (Stationary inerterval)/interval
 
    response = requests.post(postUrl, json=data, headers=self.headers)    
    print (response.text)

  def setMotionStartStop(self, deviceId):
    postUrl = self.baseURL + "/accounts/" + self.accountKey + "/beacons/configuration/" + deviceId
    #print postUrl

    #Motion Start/stop configuration, device only send Motion Start/stop events
    data = {"deviceMode":1,
        "workingMode":3,
        "sensor":1,
        "interval":900, #15 minutes in seconds,
        "interruptConfig":{"inner":5,"outter":12,"actThresh":300,"actTime":32,"inactThresh":256,"inactTime":64}}
        #outter = (Stationary inerterval)/interval
 
    response = requests.post(postUrl, json=data, headers=self.headers)    
    print (response.text)
	
  def getConfig(self, deviceId):
    postUrl = self.baseURL + "/accounts/" + self.accountKey + "/beacons/configuration/" + deviceId

    response = requests.get(postUrl, headers=self.headers)    
    print response.json()

 

api = Bewhere(your username, your password, your accountid)

api.authentication()

#Please look at https://app.swaggerhub.com/apis/BEWHEREINC_1/BeWhere/4.0.0#/DeviceConfiguration for configuration attributes

#Set timer configuration
api.setTimerConfig("357591080080689") #357591080080689 is device IMEI

#Set Motion Trip configuration(Motion Trip with continuous motion)
api.setMotionTrip("357591080080689")

#set Motion Movement configuration
api.setMotionMovement("357591080080689")

#set Motion Start/Stop configuration(Motion Trip without continuous motion)
api.setMotionStartStop("357591080080689")