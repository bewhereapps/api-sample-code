import hashlib
import requests
import time
import json

class Bewhere:
  ## Sets up the local variables for the username/password
  def __init__(self, username, password, key):
    self.username=username
    self.password=password
    self.accountKey = key

    self.baseURL = "https://api.bewhere.com"
    self.token = ""
 
  def login(self):
    url = self.baseURL + "/authentication/" + self.username + "?type=m2m"
    response = requests.get(url)
    response_data = response.json()

    hashGen = hashlib.sha256()

    # Hash our password
    hashGen.update(self.password)
    first_hash = hashGen.hexdigest()

    # Add it to the salt
    b = bytearray()
    concat = response_data["salt"]+first_hash
    b.extend(concat.encode())
    hashGen_final = hashlib.sha256()
    hashGen_final.update(b)
    final_hash = hashGen_final.hexdigest()

    self.token = response_data["token"]

    # Post data to send back
    data = {'authphrase' : final_hash,
        'username' : self.username,
        'tidHashAlgorithm' : '2',
        'tidSession' : '3'}
       
    # Set the standard headers to include the token
    self.headers = {'Token': self.token,
    'Accept': 'application/json',
    'Content-Type' : 'application/json'}

    post_url = self.baseURL + "/authentication"
    response = requests.post(post_url, json=data, headers=self.headers)
    print self.headers
    print response.text
    #print response.json()

  def getSnapshot(self):
    poll_url = self.baseURL + "/accounts/" + self.accountKey + "/snapshots"
    response = requests.get(poll_url, headers=self.headers)
    
    print response.json()

  def getBeacons(self):
    poll_url = self.baseURL + "/accounts/" + self.accountKey + "/beacons"
    response = requests.get(poll_url, headers=self.headers)

    print response.text

  def getDeviceHistory(self, deviceId):
    start = (time.time()-24*60*60) * 1000
    query = "?start=%d&end=%d" % (start, time.time()*1000)
    poll_url = self.baseURL + "/accounts/" + self.accountKey + "/streams/" + deviceId + query
    response = requests.get(poll_url, headers=self.headers)

    print response.text

  def getAllDevicesHistory(self):
    start = (time.time()-24*60*60) * 1000

    query = "&start=%d&end=%d" % (start, time.time()*1000)
    url = self.baseURL + "/accounts/" + self.accountKey + "/streams/history?limit=100%s" % query
    print "Request:  %s" % url
    #logger.log("Request:  %s" % url)
    
    historyData = requests.get(url, headers=self.headers)
    print historyData.text
 
  def getSnapshotByName(self, name):
    url = self.baseURL + "/accounts/" + self.accountKey + "/snapshots/name/" + name
    response = requests.get(url, headers=self.headers)
    print  (response.json())
	
  def updateOdometer(self, id):
    data = {
	  "distance": 5000, # 5000 meters
	  "timestamp": 1651867401000
	}  
    url = self.baseURL + "/accounts/" + self.accountKey + "/snapshots/" + id
    response = requests.put(url, json=data, headers=self.headers)
    print  response

  def updateTotalEngineHrs(self, id):
    data = {
	  "total_engine_hrs": 6000, # 6000 seconds
	  "timestamp": 1651867401000
	} 
    url = self.baseURL + "/accounts/" + self.accountKey + "/snapshots/" + id
    response = requests.put(url, json=data, headers=self.headers)
    print  response
	
  def changeAccountName(self, accountId, newName):
    #You must call the get method to retrieve the account information first.
    url = self.baseURL + "/accounts/" + accountId 
    print url
    response = requests.get(url, headers=self.headers)
    print  response
    response_data = response.json()
    response_data["name"] = newName
 
    response = requests.put(url, json=response_data, headers=self.headers)
	
    print  response	
	
  def createAccount(self, accountName):
    url = self.baseURL + "/accounts" 
    print url
    data = {
	  "name": accountName
	} 
    response = requests.post(url, json=data, headers=self.headers)
    print  response	

  def updateAccountRoutes(self, accountId):
    url = self.baseURL + "/accounts/" + accountId + "/routing";
    print url
    data = [
	  {
		"name": "route1",
		"url": "route1 url",
		"isEnabled": True,
		"headers": [
		  {
			"key": "header_key1",
			"value": "header_value1"
		  },
		  {
			"key": "header_key2",
			"value": "header_value2"
		  }
		]
	  },
	  {
		"name": "route2",
		"url": "route2 url",
		"isEnabled": True,
		"headers": [
		  {
			"key": "header_key1",
			"value": "header_value1"
		  },
		  {
			"key": "header_key2",
			"value": "header_value2"
		  }
		],
		#formatter is optional
		"formatter": "{\"sensorData3\": {\"eventType\": \"$$eventType\",\"direction\": \"$$direction\",\"navStat\": \"$$navStat\",\"mnc\": \"$$mnc\",\"speed\": \"$$speed\",\"impact\": \"$$impact\",\"rsrp\": \"$$rsrp\",\"temperature\": \"$$temperature\",\"altitude\": \"$$altitude\",\"latitude\": \"$$latitude\",\"batteryLevel\": \"$$batteryLevel\",\"aux1\": \"$$aux1\",\"aux2\": \"$$aux2\",\"aux3\": \"$$aux3\",\"aux4\": \"$$aux4\",\"aux1_json\": \"$$aux1_json\",\"sim\": \"$$sim\",\"urat\": \"$$urat\",\"hdop\": \"$$hdop\",\"timestamp\": \"$$timestamp\",\"mcc\": \"$$mcc\",\"lac\": \"$$lac\",\"pressure\": \"$$pressure\",\"locationSource\": \"$$locationSource\",\"received\": \"$$received\",\"cid\": \"$$cid\",\"numSvs\": \"$$numSvs\",\"light\": \"$$light\",\"longitude\": \"$$longitude\",\"humidity\": \"$$humidity\",\"imsi\": \"$$imsi\"},\"networkLocation\": {\"latitude\": \"$$networkLatitude\",\"longitude\": \"$$networkLongitude\",\"accuracy\": \"$$networkAccuracy\"},\"deviceInfo\": {\"imei\": \"$$imei\",\"serialNumber\": \"$$serialNumber\",\"sender\": \"$$sender\",\"version\": \"$$version\",\"name\": \"$$name\"}}"
	  }
    ];

    response = requests.put(url, json=data, headers=self.headers)
    print  response	

  def generateAccountAPIKey(self, accountId, userId):
    url = self.baseURL + "/accounts/" + accountId + "/users/" + userId  + "/apikey"
    print url
    response = requests.get(url, headers=self.headers)
    apikey = response.text
    print  apikey

  def generateDealerUserAPIKey(self, dealerId, userId):
    url = self.baseURL + "/dealers/" + dealerId + "/users/" + userId  + "/apikey"
    print url
    response = requests.get(url, headers=self.headers)
    print response
    apikey = response.text
    print  apikey

 # If you don't have API key, use login method to get temporary token, the token will expire if the token has not been used in 20 minutes 
api = Bewhere("your username", "your password", "your account id")
api.login()

#Get a list of all the devices
#Please refer to: https://app.swaggerhub.com/apis/BEWHEREINC_1/BeWhere/4.0.0#/Beacons/get_accounts__accountKey__beacons
api.getBeacons()
#Get all devices snapshot
#please refer to: https://app.swaggerhub.com/apis/BEWHEREINC_1/BeWhere/4.0.0#/Snapshots/get_accounts__accountKey__snapshots
api.getSnapshot()
#Get device 864475040072679 historical records based on device time stamp. start and end are Unix epoch time in milliseconds.
#Please refer to:  https://app.swaggerhub.com/apis/BEWHEREINC_1/BeWhere/4.0.0#/Streams/get_accounts__accountKey__streams__id_
api.getDeviceHistory("864475040072679")
#Get all devices historical records  based on server receive time. start and end are Unix epoch time in milliseconds.
#Please refer to: https://app.swaggerhub.com/apis/BEWHEREINC_1/BeWhere/4.0.0#/Streams/get_accounts__accountKey__streams_history
api.getAllDevicesHistory()
#Get Device snapshot by device name.
#Please refer to: https://app.swaggerhub.com/apis/BEWHEREINC_1/BeWhere/4.0.0#/Snapshots/get_accounts__accountKey__snapshots_name__name_
api.getSnapshotByName(deviceName);
#Update device distance
#Please refer to: https://app.swaggerhub.com/apis/BEWHEREINC_1/BeWhere/4.0.0#/Snapshots/put_accounts__accountKey__snapshots__id_       
api.updateOdometer("357591080080689");
#Update device total eengine hours
#Please refer to: https://app.swaggerhub.com/apis/BEWHEREINC_1/BeWhere/4.0.0#/Snapshots/put_accounts__accountKey__snapshots__id_       
api.updateTotalEngineHrs("357591080080689");

#Update account name
api.changeAccountName("account id", "new account name")
#Create new account
#api.createAccount("my new account")

#end 
        
        
#If you have API key, then you can use accountid and api key to send request without calling login method 
apiKey = "your api key";
api.headers = {'Token': apiKey,
    'Accept': 'application/json',
    'Content-Type' : 'application/json'}
print api.headers
#Get a list of all the devices
#Please refer to: https://app.swaggerhub.com/apis/BEWHEREINC_1/BeWhere/4.0.0#/Beacons/get_accounts__accountKey__beacons
api.getBeacons()
#Get all devices snapshot
#please refer to: https://app.swaggerhub.com/apis/BEWHEREINC_1/BeWhere/4.0.0#/Snapshots/get_accounts__accountKey__snapshots
api.getSnapshot()
#Get device 864475040072679 historical records based on device time stamp. start and end are Unix epoch time in milliseconds.
#Please refer to:  https://app.swaggerhub.com/apis/BEWHEREINC_1/BeWhere/4.0.0#/Streams/get_accounts__accountKey__streams__id_
api.getDeviceHistory("864475040072679")
#Get all devices historical records  based on server receive time. start and end are Unix epoch time in milliseconds.
#Please refer to: https://app.swaggerhub.com/apis/BEWHEREINC_1/BeWhere/4.0.0#/Streams/get_accounts__accountKey__streams_history
api.getAllDevicesHistory()
#Get Device snapshot by device name.
#Please refer to: https://app.swaggerhub.com/apis/BEWHEREINC_1/BeWhere/4.0.0#/Snapshots/get_accounts__accountKey__snapshots_name__name_
api.getSnapshotByName(deviceName);
#Update device distance
#Please refer to: https://app.swaggerhub.com/apis/BEWHEREINC_1/BeWhere/4.0.0#/Snapshots/put_accounts__accountKey__snapshots__id_       
api.updateOdometer("357591080080689");
#Update device total eengine hours
#Please refer to: https://app.swaggerhub.com/apis/BEWHEREINC_1/BeWhere/4.0.0#/Snapshots/put_accounts__accountKey__snapshots__id_       
api.updateTotalEngineHrs("357591080080689");

#Update account name
api.changeAccountName("account id", "new account name")
#Create new account
#api.createAccount("my new account")

#Update account routes
api.updateAccountRoutes("account id");
#Generate account user APIKey
api.generateAccountAPIKey("account id","user id")
#Generate dealer user APIKey
api.generateDealerUserAPIKey("dealer id","user id")

