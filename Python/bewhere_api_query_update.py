import hashlib
import requests
import time
import json

class Bewhere:
  def __init__(self, token, accountKey):
    self.accountKey = accountKey
    self.baseURL = "https://api.bewhere.com"
    self.headers = {'Token': token,
      'Accept': 'application/json',
      'Content-Type' : 'application/json'}
	
  #Query device configuration
  #Refer to https://app.swaggerhub.com/apis/BEWHEREINC_1/BeWhere/4.0.0#/Beacons/post_accounts__accountKey__beacons_configuration_query__type_
  def queryConfiguration(self):
    type = 'id' # The type will be one of the following: 'id', 'serialnumber', or 'name'.
    postUrl = self.baseURL + "/accounts/" + self.accountKey + "/beacons/configuration/query/" + type
	
	#The post data is a list of IDs if the type is 'id', a list of names if the type is 'name', and a list of serial numbers if the type is 'serialnumber'
    data = [id1,id2,id3,id4] 
    print postUrl

    response = requests.post(postUrl, json=data, headers=self.headers  )    
    print response.status_code
    print response.json()

  #Update configuration for multiple devices
  #Refer to https://app.swaggerhub.com/apis/BEWHEREINC_1/BeWhere/4.0.0#/Beacons/post_accounts__accountKey__beacons_configuration_update_single
  def updateConfiguration(self):
    postUrl = self.baseURL + "/accounts/" + self.accountKey + "/beacons/configuration/update/single"
	
    data = {
		  "configuration": {
			"deviceMode": 1,
			"workingMode": 0,
			"sensor": 0,
			"interval": "7200",
			"gpsSkip": 0,
			"gpsTimeout": 0,
			"gpsExtension": 0,
			"startFrom": "",
			"logInterval": 0,
			"interruptConfig": {
			   "inner": 0,
			   "outter": 12,
			   "actThresh": 0,
			   "actTime": 0,
			   "inactThresh": 0,
			   "inactTime": 0,
			   "modeSelector": 0,
			   "outputDataRate": 4,
			   "fullScale": 0,
			   "threshold": 6,
			   "wakeDuring": 1,
			   "sleepDuring": 0,
			   "debounce_interval": 0,
			   "debounce_initial": 0,
			   "debounce_count": 0,
			   "optimization": False
			},
			"timeslotInterval": 0,
			"advDuration": 0
		  },
		  "ids": [
			id1, id2, id3
		  ]
		}
    print postUrl

    response = requests.post(postUrl, json=data, headers=self.headers  )    
    # If all devices are  updated successfully, return HTTP status 200.
    # If all devices fail to update, return HTTP status 400.
    # If some devices fail to update but others are updated successfully, return HTTP status 206 with the message:
    # {'message': 'Some devices failed to update', 'failedDevices': [id1, id2]}.
    print response.status_code
    #print response.json()
    
  #Query device
  #Refer to https://app.swaggerhub.com/apis/BEWHEREINC_1/BeWhere/4.0.0#/Beacons/post_accounts__accountKey__beacons_query__type_
  def queryDevice(self):
    type = 'id' # The type will be one of the following: 'id', 'serialnumber', or 'name'.
    postUrl = self.baseURL + "/accounts/" + self.accountKey + "/beacons/query/" + type
	
	#The post data is a list of IDs if the type is 'id', a list of names if the type is 'name', and a list of serial numbers if the type is 'serialnumber'
    data = [id1,id2,id3,id4] 
    print postUrl

    response = requests.post(postUrl, json=data, headers=self.headers  )    
    print response.status_code
    print response.json()
	
  #Update multiple devices
  #Refer to https://app.swaggerhub.com/apis/BEWHEREINC_1/BeWhere/4.0.0#/Beacons/put_accounts__accountKey__beacons_update_single
  def updateDevice(self):
    postUrl = self.baseURL + "/accounts/" + self.accountKey + "/beacons/update/single"
	
    data = {
          "deviceInfo":{
                  "tidStatus":0  #status code: 0 - Active, 2 - Deactivated,
		  },
		  "ids": [
			id1, id2, id3
		  ]
		}
    print postUrl

    response = requests.put(postUrl, json=data, headers=self.headers  )    
    # If all devices are  updated successfully, return HTTP status 200.
    # If all devices fail to update, return HTTP status 400.
    # If some devices fail to update but others are updated successfully, return HTTP status 206 with the message:
    # {'message': 'Some devices failed to update', 'failedDevices': [id1, id2]}.
    print response.status_code
    #print response.json()	
	
api = Bewhere(your token, your accountid)

#api.queryConfiguration();
#api.updateConfiguration();
#api.queryDevice();
#api.updateDevice();
