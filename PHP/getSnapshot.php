<?php
$username = "your user name";
$password = "your password";
$account_id = "your account id";
$baseurl = 'https://api.bewhere.com';

$url_token = $baseurl."/authentication/".$username."?type=m2m";

// getting token and sale
$json = file_get_contents($url_token);
$obj = json_decode($json);
//echo $obj->access_token;

$returned_token = $obj->token;
$returned_salt = $obj->salt;
//print_r($obj);


// hash password sha256

$password_hash = hash('sha256', $password);

$passandsalt =  $returned_salt.$password_hash;

//$passandsalt_bytearray = unpack('c*', $passandsalt);

$passandsalt_hash = hash('sha256', $passandsalt);
//$passandsalt_hash = hash('sha256', $passandsalt_bytearray);
// print_r($passandsalt_hash."  name");


//post
$url = $baseurl.'/authentication';
 

//Initiate cURL.
$ch = curl_init($url);
 
//The JSON data.

$data = array(
    'authphrase'      => $passandsalt_hash,
    'username'    => $username,
	'tidHashAlgorithm' =>  '2',
	'tidSession' => '3'
	//'id'    => $account_id
);
 

$header = array(
	'Token' => $returned_token,
	'Accept' => 'application/json',
	'Content-Type'=> 'application/json'
);
//Encode the array into JSON.
$jsonDataEncoded = json_encode($data);
//Tell cURL that we want to send a POST request.
curl_setopt($ch, CURLOPT_POST, 1);
 
//Attach our encoded JSON string to the POST fields.
curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
 
//Set the content type to application/json
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Token: '.$returned_token.'','Content-Type: application/json', 'Accept: application/json')); 
curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
//Execute the request
$result = curl_exec($ch);
print_r($result);

//Get all devices snapshot.
//Doc: https://app.swaggerhub.com/apis/BEWHEREINC_1/BeWhere/4.0.0#/Snapshots/get_accounts__accountKey__snapshots
 
$beaconUrl ='/accounts/'.$account_id.'/snapshots';

$ch_Beacon = curl_init($baseurl.$beaconUrl);

curl_setopt($ch_Beacon, CURLOPT_GET, 1);
//Set the content type to application/json
curl_setopt($ch_Beacon, CURLOPT_HTTPHEADER, array('Token: '.$returned_token.'','Content-Type: application/json', 'Accept: application/json')); 
curl_setopt($ch_Beacon,CURLOPT_RETURNTRANSFER,1);
//Execute the request
$beaconHistoryResult = curl_exec($ch_Beacon);
print_r($beaconHistoryResult);
?>
