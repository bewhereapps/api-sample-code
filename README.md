# README #

 
### What is this repository for? ###
Sample code to call bewhere API 

### API Document ###
https://app.swaggerhub.com/apis/BEWHEREINC_1/BeWhere/4.0.0#/

### How do I get set up? ###

1. Find your account id from bewhere portal.
![alt text](images/account_id.png)

2. There are two ways to call bewhere api. One is using user name and password to get temporary token, the temporary token will expire if it has not been used in 20 minutes. Another way is using API key which you don't need to call Login method. Both ways have been implemented in the sample code. You can generate API kek from bewhere portal:
![alt text](images/API%20key.png)

### Sample API calls for query data###
login()- Use username and password to get temporary token.

getBeacons(accountId) - Get a list of all the devices. Please refer to: https://app.swaggerhub.com/apis/BEWHEREINC_1/BeWhere/4.0.0#/Beacons/get_accounts\__accountKey\__beacons

getSnapshot(accountId) - Get all devices snapshot. please refer to: https://app.swaggerhub.com/apis/BEWHEREINC_1/BeWhere/4.0.0#/Snapshots/get_accounts\__accountKey\__snapshots

getSnapshotByName(accountId, deviceName) - Get Device snapshot by device name Please refer to: https://app.swaggerhub.com/apis/BEWHEREINC_1/BeWhere/4.0.0#/Snapshots/get_accounts__accountKey__snapshots_name__name_ 

getDeviceHistory(accountId, deviceId, start, end) - Get device historical records based on device time stamp. start and end are Unix epoch time in milliseconds. Please refer to:  https://app.swaggerhub.com/apis/BEWHEREINC_1/BeWhere/4.0.0#/Streams/get_accounts\_\_accountKey\_\_streams\_\_id\_

getAllDevicesHistory(accountId, start, end) - Get all devices historical records  based on server receive time. start and end are Unix epoch time in milliseconds. Please refer to: https://app.swaggerhub.com/apis/BEWHEREINC_1/BeWhere/4.0.0#/Streams/get_accounts\__accountKey\__streams_history

__We suggest to use getAllDevicesHistory method to get all devices historical records.__ 

Please use following sample code to get all devices historical records 

var start_time = The current Unix epoch time  

sleep 5 minutes   

while(true) {

	var end_time = The current Unix epoch time 

	var data = get https://api.bewhere.com/accounts/{accountid}/streams/history?limit=1000&start=start_time&end=end_time

	if (data.total > 0) store data.stream to your database

	set start_time = data.maxDate

	sleep 5 minutes
}


### Set device configuration Sample API calls###
Set AT1 configuration sample code -- https://bitbucket.org/bewhereapps/api-sample-code/src/master/Python/bewhere_api_device_configuration_AT1.py

Set AT2 configuration sample code -- https://bitbucket.org/bewhereapps/api-sample-code/src/master/Python/bewhere_api_device_configuration_AT2.py

Set BeMini configuration sample code -- https://bitbucket.org/bewhereapps/api-sample-code/src/master/Python/bewhere_api_device_configuration_BeMini.py

Please refer to:  https://app.swaggerhub.com/apis/BEWHEREINC_1/BeWhere/4.0.0#/DeviceConfiguration

### Move devices to new account###
C#: MoveListofBeacons()

Java: moveListofBeacons()

Please refer to:  https://app.swaggerhub.com/apis/BEWHEREINC_1/BeWhere/4.0.0#/Beacons/put_accounts\__accountKey\_\_beacons_multi_move\_\_newaccountKey\_

If you don't know the device's account id when calling move beacons API, you can use your dealer account id.

Login into bewhere portal as a dealer user, you can find your dealer account id on the accounts selection page: 
![alt text](images/dealer_account_id.png)

### Update device Odometer 
updateOdometer - Update device Odometer. Please refer to: https://app.swaggerhub.com/apis/BEWHEREINC_1/BeWhere/4.0.0#/Snapshots/put_accounts__accountKey__snapshots__id_

### Update device Total Engine Hours
updateTotalEngineHrs - Update device Total Engine Hours. Please refer to: https://app.swaggerhub.com/apis/BEWHEREINC_1/BeWhere/4.0.0#/Snapshots/put_accounts__accountKey__snapshots__id_
