﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using BeWhere.Lib.Config;
using BeWhere.Lib.Model;
using BeWhere.Lib.Utils;
using Newtonsoft.Json;

namespace BeWhere.Lib
{
    public class SnapshotUpdateOdometer
    {
        public int distance;
        public long timestamp;
    }

    public class SnapshotUpdateTotalEngineHrs
    {
        public int total_engine_hrs;
        public long timestamp;
    }

    public class BeWhereAPI
    {
        private static HttpClient client = new HttpClient();

        public async Task<TokenAndSalt> GetTokenAndSalt(String email)
        {
            var url = BeWhereConfig.API_URL + "/authentication/" + email + "/?type=" + BeWhereConfig.TYPE;
            try
            {
                Console.WriteLine("GetTokenAndSalt Request - {0}", url);
                HttpResponseMessage response = await client.GetAsync(url);
                Console.WriteLine(response.ToString());
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    string responseBody = await response.Content.ReadAsStringAsync();
                    Console.WriteLine("GetTokenAndSalt Response - {0}", responseBody);
                    return JsonConvert.DeserializeObject<TokenAndSalt>(responseBody);
                }
                else
                {
                    Console.WriteLine(response.StatusCode);
                    Console.WriteLine("GetTokenAndSalt Response - {0}", "Failure");
                    return null;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("GetTokenAndSalt Response - {0}", "Failure");
                Console.WriteLine(ex.StackTrace.ToString());
                return null;
            }
        }

        public async Task<LoginResponse> GetToken(String userName, String password, String token, String salt)
        {
            String url = BeWhereConfig.API_URL + "/authentication";
            Byte[] saltAndPassByteArray;
            String concatString = String.Empty;
            try
            {
                using (SHA256 sha256Hash = SHA256.Create())
                {
                    byte[] passwordByteArray = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(password));
                    String hashedPass = BeWhereUtils.ByteArrayToHex(passwordByteArray);
                    concatString = salt + hashedPass;
                }

                using (SHA256 sha256Hash = SHA256.Create())
                {
                    Byte[] combined = Encoding.UTF8.GetBytes(concatString);
                    saltAndPassByteArray = sha256Hash.ComputeHash(combined);
                }
                var authphrase = BeWhereUtils.ByteArrayToHex(saltAndPassByteArray);

                TokenRequestData tokenRequestData = new TokenRequestData();
                tokenRequestData.username = userName;
                tokenRequestData.authphrase = authphrase;
                String myContent = JsonConvert.SerializeObject(tokenRequestData);

                var stringContent = new StringContent(myContent, Encoding.UTF8, "application/json");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Token", token);

                Console.WriteLine("GetToken Request - {0}", url);
                HttpResponseMessage response = await client.PostAsync(url, stringContent);
                Console.WriteLine(response.ToString());
                if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Accepted)
                {
                    string responseBody = await response.Content.ReadAsStringAsync();
                    Console.WriteLine("GetToken Response - {0}", responseBody);
                    return JsonConvert.DeserializeObject<LoginResponse>(responseBody);
                }
                else
                {
                    Console.WriteLine("GetToken Response - {0}", "Failure");
                    Console.WriteLine(response.StatusCode);
                    return null;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("GetToken Response - {0}", "Failure");
                Console.WriteLine(ex.StackTrace.ToString());
                return null;
            }
        }

        public async Task<LoginResponse> Login(String userName, String password)
        {
            TokenAndSalt tokenAndSalt = await GetTokenAndSalt(userName);
            if (tokenAndSalt != null)
            {
                LoginResponse response = await GetToken(userName, password, tokenAndSalt.token, tokenAndSalt.salt);
                return response;
            }
            return null;
        }

        public async Task GetBeacons(String accountid, String token)
        {
            client = new HttpClient();
            client.Timeout = new TimeSpan(0, 1, 0);
            String url = BeWhereConfig.API_URL + "/accounts/" + accountid + "/beacons";
            try
            {
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Token", token);
                HttpResponseMessage response = await client.GetAsync(url);
                Console.WriteLine(response.ToString());
                if (response.IsSuccessStatusCode)
                {
                    string responseBody = await response.Content.ReadAsStringAsync();
                    Console.WriteLine("Get Beacons Response - Succeed:" + responseBody);
                }
                else
                {
                    Console.WriteLine("Get Beacons Response - Failure:" + response.StatusCode.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Get Beacons Response - Failure");
                Console.WriteLine(ex.StackTrace.ToString());
            }
        }

        public async Task GetSnapshot(String accountid, String token)
        {
            client = new HttpClient();
            client.Timeout = new TimeSpan(0, 1, 0);
            String url = BeWhereConfig.API_URL + "/accounts/" + accountid + "/snapshots";
            try
            {
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Token", token);
                HttpResponseMessage response = await client.GetAsync(url);
                Console.WriteLine(response.ToString());
                if (response.IsSuccessStatusCode)
                {
                    string responseBody = await response.Content.ReadAsStringAsync();
                    Console.WriteLine("Get Snapshot Response - Succeed:" + responseBody);
                }
                else
                {
                    Console.WriteLine("Get Snapshot Response - Failure:" + response.StatusCode.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Get Snapshot Response - Failure");
                Console.WriteLine(ex.StackTrace.ToString());
            }
        }

        public async Task GetSnapshotByName(String accountid, String token, String name)
        {
            client = new HttpClient();
            client.Timeout = new TimeSpan(0, 1, 0);
            String url = BeWhereConfig.API_URL + "/accounts/" + accountid + "/snapshots/name/" + name;
            try
            {
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Token", token);
                HttpResponseMessage response = await client.GetAsync(url);
                Console.WriteLine(response.ToString());
                if (response.IsSuccessStatusCode)
                {
                    string responseBody = await response.Content.ReadAsStringAsync();
                    Console.WriteLine("Get Snapshot Response - Succeed:" + responseBody);
                }
                else
                {
                    Console.WriteLine("Get Snapshot Response - Failure:" + response.StatusCode.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Get Snapshot Response - Failure");
                Console.WriteLine(ex.StackTrace.ToString());
            }
        }

        public async Task GetDeviceHistory(String accountid, String token, String deviceId, long start, long end)
        {
            client = new HttpClient();
            client.Timeout = new TimeSpan(0, 1, 0);
            //Start: Starting Record timestamp (Epoch time) in milliseconds
            //End: Ending Record timestamp (Epoch time) in milliseconds
            String url = BeWhereConfig.API_URL + "/accounts/" + accountid + "/streams/" + deviceId + "?start=" + start.ToString() + "&end=" + end.ToString();
            try
            {
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Token", token);
                HttpResponseMessage response = await client.GetAsync(url);
                Console.WriteLine(response.ToString());
                if (response.IsSuccessStatusCode)
                {
                    string responseBody = await response.Content.ReadAsStringAsync();
                    Console.WriteLine("Get Device History Response - Succeed:" + responseBody);
                }
                else
                {
                    Console.WriteLine("Get Device History Response - Failure:" + response.StatusCode.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Get Device History Response - Failure");
                Console.WriteLine(ex.StackTrace.ToString());
            }
        }

        public async Task GetAllDevicesHistory(String accountid, String token, long start, long end)
        {
            client = new HttpClient();
            client.Timeout = new TimeSpan(0, 1, 0);
            //Start: Starting Record timestamp (Epoch time) in milliseconds
            //End: Ending Record timestamp (Epoch time) in milliseconds
            String url = BeWhereConfig.API_URL + "/accounts/" + accountid + "/streams/history?limit=1000&start=" + start.ToString() + "&end=" + end.ToString();
            try
            {
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Token", token);
                HttpResponseMessage response = await client.GetAsync(url);
                Console.WriteLine(response.ToString());
                if (response.IsSuccessStatusCode)
                {
                    string responseBody = await response.Content.ReadAsStringAsync();
                    Console.WriteLine("Get Devices History Response - Succeed:" + responseBody);
                }
                else
                {
                    Console.WriteLine("Get Devices History Response - Failure:" + response.StatusCode.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Get Devices History Response - Failure");
                Console.WriteLine(ex.StackTrace.ToString());
            }
        }

        public async Task MoveListofBeacons(String accountid, String token, String newAccountid, String[] beacons)
        {
            client = new HttpClient();
            client.Timeout = new TimeSpan(0, 1, 0);
            String url = BeWhereConfig.API_URL + "/accounts/" + accountid + "/beacons/multi/move/" + newAccountid;
            try
            {
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Token", token);

                String myContent = JsonConvert.SerializeObject(beacons);
                var stringContent = new StringContent(myContent, Encoding.UTF8, "application/json");

                Console.WriteLine("GetToken Request - {0}", url);
                HttpResponseMessage response = await client.PutAsync(url, stringContent);
                if (response.IsSuccessStatusCode)
                {
                    Console.WriteLine("Move List of Beacons Response - Succeed:" + response.StatusCode.ToString());
                }
                else
                {
                    Console.WriteLine("Move List of Beacons Response - Failure:" + response.StatusCode.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Move List of Beacons Response - Failure");
                Console.WriteLine(ex.StackTrace.ToString());
            }
        }


        public async Task UpdateOdometer(String accountid, String token, String id, SnapshotUpdateOdometer odometer)
        {
            client = new HttpClient();
            client.Timeout = new TimeSpan(0, 1, 0);
            String url = BeWhereConfig.API_URL + "/accounts/" + accountid + "/snapshots/" + id;
            try
            {
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Token", token);

                String myContent = JsonConvert.SerializeObject(odometer);
                var stringContent = new StringContent(myContent, Encoding.UTF8, "application/json");

                Console.WriteLine("GetToken Request - {0}", url);
                HttpResponseMessage response = await client.PutAsync(url, stringContent);
                if (response.IsSuccessStatusCode)
                {
                    Console.WriteLine("Update Odometer - Succeed:" + response.StatusCode.ToString());
                }
                else
                {
                    Console.WriteLine("Update Odometer - Failure:" + response.StatusCode.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Update Odometer - Failure");
                Console.WriteLine(ex.StackTrace.ToString());
            }
        }

        public async Task UpdateTotalEngineHrs(String accountid, String token, String id, SnapshotUpdateTotalEngineHrs engineHrs)
        {
            client = new HttpClient();
            client.Timeout = new TimeSpan(0, 1, 0);
            String url = BeWhereConfig.API_URL + "/accounts/" + accountid + "/snapshots/" + id;
            try
            {
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Token", token);

                String myContent = JsonConvert.SerializeObject(engineHrs);
                var stringContent = new StringContent(myContent, Encoding.UTF8, "application/json");

                Console.WriteLine("GetToken Request - {0}", url);
                HttpResponseMessage response = await client.PutAsync(url, stringContent);
                if (response.IsSuccessStatusCode)
                {
                    Console.WriteLine("Update Engine Hrs - Succeed:" + response.StatusCode.ToString());
                }
                else
                {
                    Console.WriteLine("Update Engine Hrs - Failure:" + response.StatusCode.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Update Engine Hrs - Failure");
                Console.WriteLine(ex.StackTrace.ToString());
            }
        }
        static void GetProfile()
        {
            //await client.GetAsync()
        }
    }
}
