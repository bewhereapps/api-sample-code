﻿using System;
namespace BeWhere.Lib.Model
{
    public class TokenRequestData
    {
        public TokenRequestData()
        {
            tidHashAlgorithm = 2;
            tidSession = 1;
        }
        public String username { get; set; }
        public String authphrase { get; set; }
        public int tidHashAlgorithm { get; set; }
        public int tidSession { get; set; }
    }
}
