﻿using System;
namespace BeWhere.Lib.Model
{
    public class TokenAndSalt
    {
        public TokenAndSalt()
        {

        }
        public String token { get; set; }
        public String salt { get; set; }
    }

    public class BeWhereUser
    {
        public String id { get; set; }
        public String name { get; set; }
        public int tidRole { get; set; }
        public String roleType { get; set; }
        public String roleName { get; set; }
        public int tidTimezone { get; set; }
        public String timezone { get; set; }
        public String timezoneLong { get; set; }
        public String timezoneShort { get; set; }
        public String authorizedAccountId { get; set; }
        public String authorizedAccountName { get; set; }
        public String accountId { get; set; }
        public String accountName { get; set; }
        public String lastLogin { get; set; }
        public bool isDisabled { get; set; }
        public bool isReset { get; set; }
        public bool isMetric { get; set; }
        public bool isDealer { get; set; }
    }
    public class LoginResponse
    {
        public String token { get; set; }
        public String salt { get; set; }
        public String accountId { get; set; }
        public String id { get; set; }
        public long timestamp { get; set; }
        public int tidSession { get; set; }
        public String sessionName { get; set; }
        public String username { get; set; }
        public String ipAddress { get; set; }
        public long requested { get; set; }
        public long accessed { get; set; }
        public long authorized { get; set; }
        public long ttl { get; set; }
        public BeWhereUser user { get; set; }
    }
}
