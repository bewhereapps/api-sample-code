﻿using System;
namespace BeWhere.Lib.Config
{
    public static class BeWhereConfig
    {
        public static String API_URL = "https://api.bewhere.com";
        public static int TIMEOUT = 10000;
        public static String TYPE = "M2M";
    }
}
