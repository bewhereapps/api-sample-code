﻿using System;
using BeWhere.Lib.Model;

namespace BeWhere.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            /**** If you don't have API key, use Login method to get token, the token will expire if the token has not been used in 20 minutes ****/
            String username = "your user name";
            String password = "your password";
            BeWhere.Lib.BeWhereAPI beWhereAPI = new Lib.BeWhereAPI();
            Console.WriteLine("{0} - {1}", username, password);
            LoginResponse response = beWhereAPI.Login(username, password).GetAwaiter().GetResult();
            //Get all devices
            beWhereAPI.GetBeacons(response.accountId, response.token).Wait(); 
            //Get all device snapshot
            beWhereAPI.GetSnapshot(response.accountId, response.token).Wait();
            //Get device 356726102457827 history
            long end = (long)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalMilliseconds;
            long start = (long)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalMilliseconds - 24 * 3600 * 1000;
            beWhereAPI.GetDeviceHistory(response.accountId, response.token, "356726102457827", start, end).Wait();
            //Get all devices history
            beWhereAPI.GetAllDevicesHistory(response.accountId, response.token, start, end).Wait();
            //Move device 100000000101 to new account 
            String currentAccountId = "Current AccountId";
            String newAccountId = "New AccountId";
            beWhereAPI.MoveListofBeacons(currentAccountId, response.token, newAccountId, new String[] { "100000000101" }).Wait();
            //get device snapshot by device name 
            beWhereAPI.GetSnapshotByName(response.accountId, response.token, "device name").Wait();
            //Update device distance
            BeWhere.Lib.SnapshotUpdateOdometer odometer = new BeWhere.Lib.SnapshotUpdateOdometer();
            odometer.distance = 2000; //2000 meters
            odometer.timestamp = 1651864857000; //Epoch time in milliseconds
            beWhereAPI.UpdateOdometer(response.accountId, response.token, "357591080080689", odometer).Wait();
            //Update device total engine hours
            BeWhere.Lib.SnapshotUpdateTotalEngineHrs totalEngineHrs = new BeWhere.Lib.SnapshotUpdateTotalEngineHrs();
            totalEngineHrs.total_engine_hrs = 4000; //4000 seconds
            totalEngineHrs.timestamp = 1651864857000; //Epoch time in milliseconds
            beWhereAPI.UpdateTotalEngineHrs(response.accountId, response.token, "357591080080689", totalEngineHrs).Wait();
            Console.ReadLine();

            /****end ****/

            /**** If you have API key, then you can use accountid and api key to send request without calling Login method ****/
            String apiKey = "your api key";
            String accountId = "your account id";
            //Get all devices
            beWhereAPI.GetBeacons(accountId, apiKey).Wait();
            //Get all device snapshot
            beWhereAPI.GetSnapshot(accountId, apiKey).Wait();
            //Get device 356726102457827 history
            beWhereAPI.GetDeviceHistory(accountId, apiKey, "356726102457827", start, end).Wait();
            //Get all devices history
            beWhereAPI.GetAllDevicesHistory(accountId, apiKey, start, end).Wait();
            //Move device 100000000101 to new account 
            newAccountId = "New AccountId";
            beWhereAPI.MoveListofBeacons(accountId, apiKey, newAccountId, new String[] { "100000000101" }).Wait();
            //get device snapshot by device name
            beWhereAPI.GetSnapshotByName(accountId, apiKey, "device name").Wait();
            //Update device distance
            odometer = new BeWhere.Lib.SnapshotUpdateOdometer();
            odometer.distance = 2000; //2000 meters
            odometer.timestamp = 1651864857000; //Epoch time in milliseconds
            beWhereAPI.UpdateOdometer(accountId, apiKey, "357591080080689", odometer).Wait();
            //Update device total engine hours
            totalEngineHrs = new BeWhere.Lib.SnapshotUpdateTotalEngineHrs();
            totalEngineHrs.total_engine_hrs = 4000; //4000 seconds
            totalEngineHrs.timestamp = 1651864857000; //Epoch time in milliseconds
            beWhereAPI.UpdateTotalEngineHrs(accountId, apiKey, "357591080080689", totalEngineHrs).Wait();
            Console.ReadLine();
            /****end ****/
        }
    }
}
